-- Author      : G3m7
-- Create Date : 6/6/2019 7:52:20 PM


-----------------------------------------------------------
-- placing MultiBarLeftButton1 horizontally above pet bar
-----------------------------------------------------------
local scale = 0.9
MultiBarLeftButton1:ClearAllPoints()
MultiBarLeftButton1:SetPoint("BOTTOMLEFT", MultiBarBottomLeft, "TOPLEFT", 0, 40)
MultiBarLeftButton1:SetScale(scale)
for i=2, 12 do
    local btn = _G["MultiBarLeftButton"..i]
    local prevBtn = _G["MultiBarLeftButton"..i-1]
    btn:ClearAllPoints()
    btn:SetPoint("TOPLEFT", prevBtn, "TOPRIGHT", 0, 0)
    btn:SetScale(scale)
end
-----------------------------------------------------------


local Consumables = {
["Cerebral Cortex Compound"] = 1,
["Gizzard Gum"] = 1,
["Lung Juice Cocktail"] = 1,
["R.O.I.D.S"] = 1,
["Ground Scorpok Assay"] = 1,


["Minor Healing Potion"] = 1,
["Lesser Healing Potion"] = 1,
["Healing Potion"] = 1,
["Greater Healing Potion"] = 1,
["Superior Healing Potion"] = 1,
["Discolored Healing Potion"] = 1,
["Major Healing Potion"] = 1,
["Combat Healing Potion"] = 1,
["Minor Mana Potion"] = 1,
["Lesser Mana Potion"] = 1,
["Mana Potion"] = 1,
["Greater Mana Potion"] = 1,
["Superior Mana Potion"] = 1,
["Major Mana Potion"] = 1,
["Combat Mana Potion"] = 1,
["Restoring Balm"] = 1,
["Ice Cold Milk"] = 1,
["Melon Juice"] = 1,
["Flash Bundle"] = 1,
["Fishliver Oil"] = 1,
["Minor Rejuvenation Potion"] = 1,
["Swiftness Potion"] = 1,
["Weak Troll's Blood Potion"] = 1,
["Minor Magic Resistance Potion"] = 1,
["Limited Invulnerability Potion"] = 1,
["Strong Troll's Blood Potion"] = 1,
["Lesser Invisibility Potion"] = 1,
["Mighty Troll's Blood Potion"] = 1,
["Rage Potion"] = 1,
["Great Rage Potion"] = 1,
["Free Action Potion"] = 1,
["Shadow Protection Potion"] = 1,
["Fire Protection Potion"] = 1,
["Frost Protection Potion"] = 1,
["Holy Protection Potion"] = 1,
["Nature Protection Potion"] = 1,
["Swim Speed Potion"] = 1,
["Magic Resistance Potion"] = 1,
["Wildvine Potion"] = 1,
["Invisibility Potion"] = 1,
["Dreamless Sleep Potion"] = 1,
["Mighty Rage Potion"] = 1,
["Greater Stoneshield Potion"] = 1,
["Greater Frost Protection Potion"] = 1,
["Greater Fire Protection Potion"] = 1,
["Greater Nature Protection Potion"] = 1,
["Greater Shadow Protection Potion"] = 1,
["Greater Arcane Protection Potion"] = 1,
["Purification Potion"] = 1,
["Major Rejuvenation Potion"] = 1,
["Shadowy Potion"] = 1,
["Greater Dreamless Sleep Potion"] = 1,
["Major Troll's Blood Potion"] = 1,
["Mageblood Potion"] = 1,
["Living Action Potion"] = 1,
["Restorative Potion"] = 1,
["Scroll of Strength"] = 1,
["Scroll of Intellect"] = 1,
["Scroll of Stamina"] = 1,
["Scroll of Spirit"] = 1,
["Scroll of Agility II"] = 1,
["Scroll of Protection II"] = 1,
["Scroll of Stamina II"] = 1,
["Scroll of Spirit II"] = 1,
["Scroll of Strength II"] = 1,
["Scroll of Intellect II"] = 1,
["Scroll of Agility"] = 1,
["Scroll of Protection"] = 1,
["Scroll of Intellect III"] = 1,
["Scroll of Protection III"] = 1,
["Scroll of Stamina III"] = 1,
["Scroll of Spirit III"] = 1,
["Scroll of Agility III"] = 1,
["Scroll of Strength III"] = 1,
["Scroll of Myzrael"] = 1,
["Scroll of Messaging"] = 1,
["Scroll of Protection IV"] = 1,
["Scroll of Spirit IV"] = 1,
["Scroll of Stamina IV"] = 1,
["Scroll of Intellect IV"] = 1,
["Scroll of Agility IV"] = 1,
["Scroll of Strength IV"] = 1,
["Alterac Manna Biscuit"] = 1,
["Alterac Swiss"] = 1,
["Arathi Basin Enriched Ration"] = 1,
["Arathi Basin Field Ration"] = 1,
["Arathi Basin Iron Ration"] = 1,
["Bad Egg Nog"] = 1,
["Baked Salmon"] = 1,
["Barbecued Buzzard Wing"] = 1,
["Bean Soup"] = 1,
["Beer Basted Boar Ribs"] = 1,
["Bellara's Nutterbar"] = 1,
["Big Bear Steak"] = 1,
["Blended Bean Brew"] = 1,
["Blood Sausage"] = 1,
["Bloodbelly Fish"] = 1,
["Blue Firework"] = 1,
["Bogling Root"] = 1,
["Boiled Clams"] = 1,
["Bottle of Pinot Noir"] = 1,
["Bottled Winterspring Water"] = 1,
["Brilliant Smallfish"] = 1,
["Bristle Whisker Catfish"] = 1,
["Bubbling Water"] = 1,
--["Burning Charm"] = 1,
["Cabbage Kimchi"] = 1,
["Cactus Apple Surprise"] = 1,
["Candy Bar"] = 1,
["Candy Cane"] = 1,
["Carrion Surprise"] = 1,
["Charred Wolf Meat"] = 1,
["Cheap Beer"] = 1,
["Cherry Grog"] = 1,
["Chocolate Square"] = 1,
["Clam Chowder"] = 1,
["Clamlette Surprise"] = 1,
["Cooked Crab Claw"] = 1,
["Cooked Glossy Mightfish"] = 1,
["Cowardly Flight Potion"] = 1,
["Coyote Steak"] = 1,
["Crab Cake"] = 1,
--["Cresting Charm"] = 1,
["Crispy Bat Wing"] = 1,
["Crispy Lizard Tail"] = 1,
["Crocolisk Gumbo"] = 1,
["Crocolisk Steak"] = 1,
["Crunchy Frog"] = 1,
["Crystal Basilisk Spine"] = 1,
["Crystal Flake Throat Lozenge"] = 1,
["Cured Ham Steak"] = 1,
["Curiously Tasty Omelet"] = 1,
["Dalaran Sharp"] = 1,
["Dark Dwarven Lager"] = 1,
["Darkclaw Lobster"] = 1,
["Darkmoon Dog"] = 1,
["Darkmoon Faire Prize Ticket"] = 1,
["Darkmoon Special Reserve"] = 1,
["Darkshore Grouper"] = 1,
["Darnassian Bleu"] = 1,
["Darnassus Kimchi Pie"] = 1,
["Decoded Twilight Text"] = 1,
["Deep Fried Candybar"] = 1,
["Deep Fried Plantains"] = 1,
["Deeprun Rat Kabob"] = 1,
["Defiler's Enriched Ration"] = 1,
["Defiler's Field Ration"] = 1,
["Defiler's Iron Ration"] = 1,
["Delicious Cave Mold"] = 1,
["Dig Rat Stew"] = 1,
["Dirge's Kickin' Chimaerok Chops"] = 1,
["Dried King Bolete"] = 1,
["Dry Pork Ribs"] = 1,
["Dwarven Mild"] = 1,
["Egg Nog"] = 1,
["Elixir of Tongues (NYI)"] = 1,
["Elixir of Water Walking"] = 1,
["Enchanted Water"] = 1,
["Enriched Manna Biscuit"] = 1,
["Evermurky"] = 1,
["Explosive Rocket"] = 1,
["Fertile Bulb"] = 1,
["Festival Firecracker"] = 1,
["Filet of Redgill"] = 1,
["Fillet of Frenzy"] = 1,
["Fine Aged Cheddar"] = 1,
["Fissure Plant"] = 1,
["Fizzy Energy Drink"] = 1,
["Fizzy Faire Drink"] = 1,
["Flagon of Mead"] = 1,
["Forest Mushroom Cap"] = 1,
["Freshly Baked Bread"] = 1,
["Freshly-Squeezed Lemonade"] = 1,
["Friendship Bread"] = 1,
["Frog Leg Stew"] = 1,
["Giant Clam Scorcho"] = 1,
["Gift of Arthas"] = 1,
["Gingerbread Cookie"] = 1,
["Goblin Deviled Clams"] = 1,
["Goldenbark Apple"] = 1,
["Goldthorn Tea"] = 1,
["Gooey Spider Cake"] = 1,
["Goretusk Liver Pie"] = 1,
["Graccu's Homemade Meat Pie"] = 1,
["Greater Holy Protection Potion"] = 1,
["Green Firework"] = 1,
["Green Garden Tea"] = 1,
["Green Tea Leaf"] = 1,
["Grilled King Crawler Legs"] = 1,
["Grilled Squid"] = 1,
["Grim Guzzler Boar"] = 1,
["Haunch of Meat"] = 1,
["Healing Herb"] = 1,
["Heaven Peach"] = 1,
["Heavy Crocolisk Stew"] = 1,
["Heavy Kodo Stew"] = 1,
["Heavy Leather Ball"] = 1,
["Herb Baked Egg"] = 1,
["Highlander's Enriched Ration"] = 1,
["Highlander's Field Ration"] = 1,
["Highlander's Iron Ration"] = 1,
["Holiday Cheesewheel"] = 1,
["Holiday Spirits"] = 1,
["Homemade Cherry Pie"] = 1,
["Hot Lion Chops"] = 1,
["Hot Smoked Bass"] = 1,
["Hot Wolf Ribs"] = 1,
["Jug of Bourbon"] = 1,
["Jungle Remedy"] = 1,
["Jungle Stew"] = 1,
["Junglevine Wine"] = 1,
["Kaldorei Spider Kabob"] = 1,
["Large Raw Mightfish"] = 1,
["Lean Venison"] = 1,
["Lean Wolf Steak"] = 1,
["Leg Meat"] = 1,
["Lobster Stew"] = 1,
["Loch Frenzy Delight"] = 1,
["Lollipop"] = 1,
["Longjaw Mud Snapper"] = 1,
["Magic Candle"] = 1,
["Magic Dust"] = 1,
["Mightfish Steak"] = 1,
["Mithril Head Trout"] = 1,
["Mixed Berries"] = 1,
["Moist Cornbread"] = 1,
["Molasses Firewater"] = 1,
["Monster Omelet"] = 1,
["Moon Harvest Pumpkin"] = 1,
["Moonberry Juice"] = 1,
["Moonbrook Riot Taffy"] = 1,
["Moonglow"] = 1,
["Morning Glory Dew"] = 1,
["Mug of Shimmer Stout"] = 1,
["Mulgore Spice Bread"] = 1,
["Murloc Fin Soup"] = 1,
["Mutton Chop"] = 1,
["Mystery Stew"] = 1,
["Nightfin Soup"] = 1,
["Oil Covered Fish"] = 1,
["Oil of Immolation"] = 1,
["Oil of Olaf"] = 1,
["Pickled Kodo Foot"] = 1,
["Potion of Fervor"] = 1,
["Radish Kimchi"] = 1,
["Rainbow Fin Albacore"] = 1,
["Raptor Punch"] = 1,
["Raw Black Truffle"] = 1,
["Raw Brilliant Smallfish"] = 1,
["Raw Bristle Whisker Catfish"] = 1,
["Raw Glossy Mightfish"] = 1,
["Raw Greater Sagefish"] = 1,
["Raw Loch Frenzy"] = 1,
["Raw Longjaw Mud Snapper"] = 1,
["Raw Mithril Head Trout"] = 1,
["Raw Nightfin Snapper"] = 1,
["Raw Rainbow Fin Albacore"] = 1,
["Raw Redgill"] = 1,
["Raw Sagefish"] = 1,
["Raw Slitherskin Mackerel"] = 1,
["Raw Spinefin Halibut"] = 1,
["Raw Spotted Yellowtail"] = 1,
["Raw Summer Bass"] = 1,
["Razorlash Root"] = 1,
["Really Sticky Glue"] = 1,
["Red Firework"] = 1,
["Red Fireworks Rocket"] = 1,
["Red Hot Wings"] = 1,
["Red Streaks Firework"] = 1,
["Red, White and Blue Firework"] = 1,
["Red-speckled Mushroom"] = 1,
["Redridge Goulash"] = 1,
["Refreshing Spring Water"] = 1,
["Rhapsody Malt"] = 1,
["Ripe Watermelon"] = 1,
["Roast Raptor"] = 1,
["Roasted Boar Meat"] = 1,
["Roasted Kodo Meat"] = 1,
["Roasted Quail"] = 1,
["Runn Tum Tuber"] = 1,
["Runn Tum Tuber Surprise"] = 1,
["Sagefish Delight"] = 1,
["Sauteed Sunfish"] = 1,
["Savory Deviate Delight"] = 1,
["Scorpid Surprise"] = 1,
["Seasoned Wolf Kabob"] = 1,
["Senggin Root"] = 1,
["Severed Voodoo Claw"] = 1,
["Shinsollo"] = 1,
["Shiny Red Apple"] = 1,
["Sickly Looking Fish"] = 1,
["Skin of Dwarven Stout"] = 1,
["Slitherskin Mackerel"] = 1,
["Slumber Sand"] = 1,
["Small Pumpkin"] = 1,
["Smoked Bear Meat"] = 1,
["Smoked Desert Dumplings"] = 1,
["Smoked Sagefish"] = 1,
["Snake Bloom Firework"] = 1,
["Snake Burst Firework"] = 1,
["Snapvine Watermelon"] = 1,
["Soft Banana Bread"] = 1,
["Soothing Turtle Bisque"] = 1,
["Spiced Beef Jerky"] = 1,
["Spiced Chili Crab"] = 1,
["Spiced Wolf Meat"] = 1,
["Spiced Wolf Ribs"] = 1,
["Spicy Beefstick"] = 1,
["Spider Sausage"] = 1,
["Spinefin Halibut"] = 1,
["Spongy Morel"] = 1,
["Spotted Yellowtail"] = 1,
["Sprouted Frond"] = 1,
["Steamed Mandu"] = 1,
["Steamwheedle Fizzy Spirits"] = 1,
["Stormstout"] = 1,
["Stormwind Brie"] = 1,
["Strider Stew"] = 1,
["Striped Yellowtail"] = 1,
["Styleen's Sour Suckerpop"] = 1,
["Succulent Pork Ribs"] = 1,
["Super Snuff"] = 1,
["Sweet Nectar"] = 1,
["Tasty Lion Steak"] = 1,
["Tel'Abim Banana"] = 1,
["Tender Wolf Steak"] = 1,
["Test Food"] = 1,
["Test Stationery"] = 1,
--["Thundering Charm"] = 1,
["Tigule and Foror's Strawberry Ice Cream"] = 1,
["Tough Hunk of Bread"] = 1,
["Tough Jerky"] = 1,
["Un'Goro Etherfruit"] = 1,
["Undermine Clam Chowder"] = 1,
["Versicolor Treat"] = 1,
["Warsong Gulch Enriched Ration"] = 1,
["Warsong Gulch Field Ration"] = 1,
["Warsong Gulch Iron Ration"] = 1,
["Westfall Stew"] = 1,
["Wild Hog Shank"] = 1,
["Wild Ricecake"] = 1,
["Winter Kimchi"] = 1,
["Winter Squid"] = 1,
["Wizbang's Special Brew"] = 1,
["Yellow Rose Firework"] = 1,
["Goblin Fishing Pole"] = 1,

}


local function GetItemnameFromLink(link)
    local x = string.find(link, "%[") + 1;
    local y = string.find(link, "%]", x) -1;
    return string.sub(link, x, y)
end

local function IsItemOnBar(itemName)
    for i=37, 48 do
        local _,id = GetActionInfo(i)
        if id ~= nil then
            local actionName = GetItemInfo(id)
            if actionName == itemName then 
                return true
            end
        end
    end
    return false
end

local function GetFreeSlotOnBar()
    -- first look for an unused slot
    for i=37, 48 do
        local _,id = GetActionInfo(i)
        if id == nil then
            return i
        elseif GetActionCount(i) == 0 then
            PickupAction(i)
            ClearCursor()
            return i
        end
    end
    return nil
end

local function GetInventoryItemInfo(itemlink)
    for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local item = GetContainerItemLink(bag,slot)
			if item ~= nil then
                if GetItemnameFromLink(itemlink) == GetItemnameFromLink(item) then
                    return bag, slot
                end
			end
		end
	end
    return nil
end

local ConsumableQueue = { }
local function ItemLooted()
    for i=getn(ConsumableQueue), 1, -1 do
        local itemlink = ConsumableQueue[i]
        table.remove(ConsumableQueue, i)
        local itemName = GetItemnameFromLink(itemlink)
        if Consumables[itemName] ~= nil then 
            if IsItemOnBar(itemName) ~= true then
                -- find a free slot for the item and place it
                local actionslot = GetFreeSlotOnBar()
                if actionslot == nil then
                    GuideWarning("Consumable bar is full, cannot place item: "..itemlink)
                    return
                end
                local bag,slot=GetInventoryItemInfo(itemlink)
                if bag == nil then
                    GuideWarning("Consumable bar could not find bagitem: "..itemlink)
                    return
                end
                --GuideWarning("Placing "..itemlink.." on actionbar "..actionslot)
                PickupContainerItem(bag, slot)
                PlaceAction(actionslot)
            end
        end
    end
end

local ConsumFrame = CreateFrame("Frame")
ConsumFrame:RegisterEvent("PLAYER_REGEN_ENABLED")
ConsumFrame:RegisterEvent("CHAT_MSG_LOOT")
ConsumFrame:RegisterEvent("BAG_UPDATE")
function ConsumFrame.OnEvent(self, event, arg1, arg2, arg3, arg4)
    if event == "PLAYER_REGEN_ENABLED" then
        ItemLooted()
    elseif event == "BAG_UPDATE" then
        if InCombatLockdown() ~= true then
            ItemLooted()
        end
    elseif event == "CHAT_MSG_LOOT" then
        local arg1lower = string.lower(arg1)
        if string.find(arg1lower, "you receive loot: ") ~= nil -- looting a mob
        or string.find(arg1lower, "you receive item: ") ~= nil then-- buying from vendor
            table.insert(ConsumableQueue, arg1)
        end
    end
end
ConsumFrame:SetScript("OnEvent", ConsumFrame.OnEvent)

local RefreshBtn = CreateFrame("Button",nil, nil, "UIPanelCloseButton")
RefreshBtn:SetPoint("TOPRIGHT", MultiBarLeftButton1, "TOPLEFT", 10, 20)
RefreshBtn:SetSize(22 ,22) -- width, height
RefreshBtn:SetPoint("CENTER")
RefreshBtn:Show()
RefreshBtn:SetScript("OnClick", function()
    for i = 37,48 do
        PickupAction(i)
        ClearCursor()
	end
    for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local itemlink = GetContainerItemLink(bag,slot)
            table.insert(ConsumableQueue, itemlink)
		end
	end
    ItemLooted()
end)
