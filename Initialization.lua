-- Author      : G3m7
-- Create Date : 6/9/2019 1:58:04 PM

local Frame = CreateFrame("Frame")
Frame.isInitialized = nil
Frame.updateFrequency = 1.0
Frame.timeSinceLastUpdate = 0
Frame:RegisterEvent("PLAYER_LOGIN")

Frame:SetScript("OnEvent", function(self, event)
    if event == "PLAYER_LOGIN" then
        Frame.isInitialized = false
    end
end)

Frame:SetScript("OnUpdate", function(self, elapsed)
    if self.isInitialized == nil or self.isInitialized == true then return end

    self.timeSinceLastUpdate = self.timeSinceLastUpdate + elapsed
    if (self.timeSinceLastUpdate > self.updateFrequency and InCombatLockdown() ~= true) then
        self.isInitialized = true
        print("GemtUnitFrames initializing")
        GUF_InitChatFrame()
        GUF_InitUnitFrames()
        GUF_InitWorldMap()
        print("GemtUnitFrames finished initializing")
        self.timeSinceLastUpdate = 0;
    end
end)