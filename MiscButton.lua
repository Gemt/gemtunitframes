-- Author      : G3m7
-- Create Date : 6/16/2019 5:18:52 PM

local MiscButton=CreateFrame("Button", "MiscButton", UIParent, "SecureActionButtonTemplate");

MiscButton:RegisterForClicks("AnyUp");
MiscButton:SetAttribute("type","macro");
local macroText = ""
macroText = macroText.."/use Big-mouth Clam"
macroText = macroText.."\n/use Small Barnacled Clam"
macroText = macroText.."\n/use Thick-shelled Clam"

macroText = macroText.."\n/use Oozing Bag"
macroText = macroText.."\n/use Scum Covered Bag"
macroText = macroText.."\n/use Egg Crate"
macroText = macroText.."\n/use Box of Spells"
macroText = macroText.."\n/use Box of Rations"

macroText = macroText.."\n/use Pirate's Footlocker"
macroText = macroText.."\n/use Gadgetzan Water Co. Care Package"
macroText = macroText.."\n/use Evergreen Herb Casing"
macroText = macroText.."\n/use Torwa's Pouch"
macroText = macroText.."\n/use Cenarion Circle Cache"

--macroText = macroText.."\n/use A Small Pack"
--macroText = macroText.."\n/use Hoard of the Black Dragonflight"
--macroText = macroText.."\n/use Package of Empty Ooze Containers"
--macroText = macroText.."\n/use Bag of Empty Ooze Containers"

MiscButton:SetAttribute("macrotext",macroText);

--_G["BINDING_NAME_CLICK MacroButton:LeftButton"]="Use Item";

MiscButton:RegisterEvent("PLAYER_ENTERING_WORLD")
MiscButton:SetScript("OnEvent", function(self, event)
    if event == "PLAYER_ENTERING_WORLD" then
        SetBinding("SHIFT-A", "CLICK MiscButton:LeftButton")
        SetBinding("ALT-A", "CLICK MiscButton:LeftButton")
    end
end)