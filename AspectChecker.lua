-- Author      : G3m7
-- Create Date : 6/15/2019 12:57:39 PM

local Frame = CreateFrame("frame")
Frame:SetBackdrop{
	bgFile = "Interface\\BUTTONS\\WHITE8X8",
	insets = { left = 1, right = 1, top = 1, bottom = 1	 }
}
Frame:SetBackdropColor(1, 1, 0, 1)
local xOffset = 0
local yOffset = -100
local radius = 10
Frame:SetPoint("TOPLEFT", "UIParent", "CENTER", radius+xOffset, radius+yOffset)
Frame:SetPoint("BOTTOMRIGHT", "UIParent", "CENTER", -radius+xOffset, -radius+yOffset)

Frame:RegisterEvent("UNIT_AURA")
Frame:RegisterEvent("PLAYER_DEAD")
Frame:RegisterEvent("PLAYER_ENTERING_WORLD")

local function HasAspect()
    if IsMounted() then return true end
    for i=1,40 do
        local name, rank, icon, count, debuffType, duration, expirationTime, unitCaster, isStealable, 
        shouldConsolidate, spellId = UnitBuff("player", i)
        if name ~= nil then
            local nameLower = string.lower(name)
            if string.find(nameLower, "aspect of the ") ~= nil or string.find(nameLower, "nightsaber") ~= nil then 
                return true
            end
        end
    end
    return false
end

local enabled = true
Frame:SetScript("OnEvent", function(self, event, arg1)
    if enabled == false then return end
    if event == "UNIT_AURA" and arg1 ~= "player" then return end
    if event == "PLAYER_DEAD" then
        Frame:Hide()
        return 
    end
    if UnitLevel("player") < 6 or UnitIsDead("player") then 
        Frame:Hide()
        return 
    end
    if HasAspect() then
        Frame:Hide()
    else
        Frame:Show()
    end
end)

SLASH_ASPECT1 = '/aspect'
function SlashCmdList.ASPECT(msg, editbox)
    if enabled then
        Frame:Hide()
        enabled = false
        print("AspectChecked Disabled")
    else 
        enabled = true 
        if HasAspect() then
            Frame:Hide()
        else
            Frame:Show()
        end
        print("AspectChecked Enabled")
    end
end
