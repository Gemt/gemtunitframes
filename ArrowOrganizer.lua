-- Author      : G3m7
-- Create Date : 6/16/2019 6:05:31 PM

local Frame = CreateFrame("Frame")
Frame:RegisterEvent("PLAYER_LOGIN")
Frame:RegisterEvent("BAG_UPDATE")


local IsRepacking = false
local RepackDelta = GetTime()
local head=0
local tail=0
local LastRepack = GetTime()

local function RepackQuiver()
    if GetTime() < (LastRepack+1) then return end
    IsRepacking = true
    head = 1
    tail = GetContainerNumSlots(4)

end

Frame:SetScript("OnUpdate", function()
    if IsRepacking == false then return end
    if GetTime() < (RepackDelta+0.1) then return end
    RepackDelta = GetTime()
    local t = 1

    while( (tail > head) and (select(2,GetContainerItemInfo(4, tail)) == 200) ) do
        tail = tail -1
    end
    
    if tail > 1 then
        print("Repacking "..tail.." using "..head)
        PickupContainerItem(4,head)
        PickupContainerItem(4,tail)
        if GetContainerItemID(4, head) == nil then
            head = head + 1
        end
    end

    if tail <= head then 
        IsRepacking = false
    end
end)

Frame:SetScript("OnEvent", function(self, event, arg1)
    
    if event == "BAG_UPDATE" then
        if arg1 ~= 4 then return end
        local freeArrowSlots = GetContainerNumFreeSlots(4)
        if freeArrowSlots == 0 then 
            RepackQuiver()
            return 
        end

        local lvl = UnitLevel("player")
        local arrowId
        if lvl >= 40 then arrowId = 11285
        elseif lvl >= 25 then arrowId = 3030
        elseif lvl >= 10 then arrowId = 2515
        end
        local quiverSize = GetContainerNumSlots(4)*200
        local numArrows = GetItemCount(arrowId)
        -- this should mean, unless we are messing around with bags manually,
        -- that we dont need to scan bags for any additional arrows, as they are
        -- all in the quiver. I think?
        if (numArrows+210) < quiverSize then 
            return 
        end

        for bag=0,3 do
            for slot=1,GetContainerNumSlots(bag) do
                local itemId = GetContainerItemID(bag,slot)
                if itemId == arrowId then
                    PickupContainerItem(bag,slot)
                    PutItemInBag(23)

                    freeArrowSlots = freeArrowSlots - 1
                    if freeArrowSlots == 0 then return end
                end
            end
        end
    elseif event == "PLAYER_LOGIN" then
        -- place the starter quiver in 4th bag slot on login
        local bag1Id = GetInventoryItemID("player", 20)
        if bag1Id ~= nil and bag1Id == 2101 then
            PickupInventoryItem(20) 
            PickupInventoryItem(23)
        end
    end
end)

--1000 quiver size
--1 free slot
--800 arrows