-- Author      : G3m7
-- Create Date : 6/16/2019 5:18:52 PM


local FoodOverviewFrame = CreateFrame("Frame", "FoodOverviewFrame", UIParent)
FoodOverviewFrame:SetPoint("BOTTOMRIGHT", UIParent, "BOTTOMRIGHT", -452, -0)
FoodOverviewFrame:SetWidth(150)
FoodOverviewFrame:SetHeight(50)

FoodOverviewFrame:SetBackdrop{
	bgFile = "Interface\\BUTTONS\\WHITE8X8",
	insets = { left = 1, right = 1, top = 1, bottom = 1	 }
}
FoodOverviewFrame:SetBackdropColor(0, 0, 0, 0.5)
FoodOverviewFrame:SetClampedToScreen(true)
FoodOverviewFrame:SetMovable(true)
FoodOverviewFrame:EnableMouse(true)
FoodOverviewFrame:SetUserPlaced(true)
FoodOverviewFrame:RegisterForDrag("LeftButton")
FoodOverviewFrame:SetScript("OnDragStart", function(self)
    if IsAltKeyDown() then self:StartMoving() end
end)
FoodOverviewFrame:SetScript("OnDragStop", function(self)
    self:StopMovingOrSizing()
end)

FoodOverviewFrame.text = FoodOverviewFrame:CreateFontString(nil, "ARTWORK")
FoodOverviewFrame.text:SetFont("Fonts\\FRIZQT__.TTF", 10)    
FoodOverviewFrame.text:SetPoint("TOPLEFT", FoodOverviewFrame, "TOPLEFT", 5, -5)
FoodOverviewFrame.text:SetPoint("BOTTOMRIGHT", FoodOverviewFrame, "BOTTOMRIGHT", -5, 5)

FoodOverviewFrame.text:SetWidth(150)
FoodOverviewFrame.text:SetJustifyH("LEFT")
--FoodOverviewFrame.text:SetText("test")

local MacroButton=CreateFrame("Button", "FeedButton", UIParent, "SecureActionButtonTemplate");

MacroButton:RegisterForClicks("AnyUp");
MacroButton:SetAttribute("type","macro");
local macroText = "/cast Feed Pet"

local ValidFood = {
["Tough Jerky"]={ilvl=5,minlvl=1},
["Raw Brilliant Smallfish"]={ilvl=5,minlvl=1},
["Brilliant Smallfish"]={ilvl=5,minlvl=1},
["Raw Slitherskin Mackerel"]={ilvl=5,minlvl=1},
["Charred Wolf Meat"]={ilvl=5,minlvl=1},
["Leg Meat"]={ilvl=5,minlvl=1},
["Slitherskin Mackerel"]={ilvl=5,minlvl=1},
["Roasted Boar Meat"]={ilvl=7,minlvl=1},
["Raw Longjaw Mud Snapper"]={ilvl=15,minlvl=5},
["Loch Frenzy Delight"]={ilvl=15,minlvl=5},
["Raw Loch Frenzy"]={ilvl=15,minlvl=5},
["Raw Rainbow Fin Albacore"]={ilvl=15,minlvl=5},
["Oil Covered Fish"]={ilvl=15,minlvl=5},
["Smoked Bear Meat"]={ilvl=15,minlvl=5},
["Darkshore Grouper"]={ilvl=15,minlvl=5},
["Rainbow Fin Albacore"]={ilvl=15,minlvl=5},
["Longjaw Mud Snapper"]={ilvl=15,minlvl=5},
["Haunch of Meat"]={ilvl=15,minlvl=5},
["Deeprun Rat Kabob"]={ilvl=15,minlvl=5},
["Dig Rat Stew"]={ilvl=20,minlvl=10},
["Raw Bristle Whisker Catfish"]={ilvl=25,minlvl=15},
["Bristle Whisker Catfish"]={ilvl=25,minlvl=15},
["Mutton Chop"]={ilvl=25,minlvl=15},
["Raw Mithril Head Trout"]={ilvl=35,minlvl=25},
["Mithril Head Trout"]={ilvl=35,minlvl=25},
["Raw Rockscale Cod"]={ilvl=35,minlvl=25},
["Wild Hog Shank"]={ilvl=35,minlvl=25},
["Rockscale Cod"]={ilvl=35,minlvl=25},
["Striped Yellowtail"]={ilvl=45,minlvl=35},
["Cured Ham Steak"]={ilvl=45,minlvl=35},
["Raw Spotted Yellowtail"]={ilvl=45,minlvl=35},
["Raw Sunscale Salmon"]={ilvl=45,minlvl=35},
["Raw Nightfin Snapper"]={ilvl=45,minlvl=35},
["Raw Redgill"]={ilvl=45,minlvl=35},
["Raw Summer Bass"]={ilvl=45,minlvl=35},
["Raw Glossy Mightfish"]={ilvl=45,minlvl=35},
["Spotted Yellowtail"]={ilvl=45,minlvl=35},
["Roasted Quail"]={ilvl=55,minlvl=45},
["Large Raw Mightfish"]={ilvl=55,minlvl=45},
["Raw Whitescale Salmon"]={ilvl=55,minlvl=45},
["Grim Guzzler Boar"]={ilvl=55,minlvl=45},
["Raw Spinefin Halibut"]={ilvl=55,minlvl=45},
["Spinefin Halibut"]={ilvl=55,minlvl=45},
["Darkclaw Lobster"]={ilvl=55,minlvl=45},
["Stringy Wolf Meat"]={ilvl=5,minlvl=0},
["Crag Boar Rib"]={ilvl=5,minlvl=0},
["Chunk of Boar Meat"]={ilvl=5,minlvl=0},
["Meaty Bat Wing"]={ilvl=5,minlvl=0},
["Stringy Vulture Meat"]={ilvl=10,minlvl=0},
["Coyote Meat"]={ilvl=10,minlvl=0},
["Kodo Meat"]={ilvl=10,minlvl=0},
["Bear Meat"]={ilvl=11,minlvl=0},
["Goretusk Liver"]={ilvl=12,minlvl=0},
["Goretusk Snout"]={ilvl=12,minlvl=0},
["Crawler Claw"]={ilvl=13,minlvl=0},
["Crocolisk Meat"]={ilvl=14,minlvl=0},
["Boar Ribs"]={ilvl=14,minlvl=0},
["Clam Meat"]={ilvl=14,minlvl=0},
["Soft Frenzy Flesh"]={ilvl=15,minlvl=0},
["Crisp Spider Meat"]={ilvl=15,minlvl=0},
["Lean Wolf Flank"]={ilvl=19,minlvl=0},
["Big Bear Meat"]={ilvl=21,minlvl=0},
["Thunder Lizard Tail"]={ilvl=22,minlvl=0},
["Tangy Clam Meat"]={ilvl=23,minlvl=0},
["Stag Meat"]={ilvl=23,minlvl=0},
["Lion Meat"]={ilvl=23,minlvl=0},
["Tender Crocolisk Meat"]={ilvl=23,minlvl=0},
["Tiger Meat"]={ilvl=30,minlvl=0},
["Mystery Meat"]={ilvl=30,minlvl=0},
["Raptor Flesh"]={ilvl=30,minlvl=0},
["Red Wolf Meat"]={ilvl=30,minlvl=0},
["Heavy Kodo Meat"]={ilvl=35,minlvl=0},
["Giant Clam Meat"]={ilvl=35,minlvl=0},
["White Spider Meat"]={ilvl=40,minlvl=0},
["Tender Crab Meat"]={ilvl=40,minlvl=0},
["Tender Wolf Meat"]={ilvl=40,minlvl=0},
["Zesty Clam Meat"]={ilvl=45,minlvl=0},
["Sandworm Meat"]={ilvl=55,minlvl=0},
["Tender Crocolisk Meat"]={ilvl=23,minlvl=0},
}

MacroButton:SetAttribute("macrotext",macroText);

MacroButton:RegisterEvent("PLAYER_ENTERING_WORLD")
MacroButton:RegisterEvent("BAG_UPDATE")


local currentFood = nil
local NextBagUpdate = 0
local WaitingForBagCheck = false

local function SetNewFood()

    local petLvl = UnitLevel("pet")
    -- if petLvl is 0 it means no pet summoned and we fallback to player lvl
    if petLvl == 0 then
        petLvl = UnitLevel("player") 
    end

    local FoodCounts = {}
    local lowestCount = 99999
    local lowestShitFoodCount = 99999
    local lowestShitFood = nil
    local oldCurrentFood = currentFood
    local goodCount=0
    local decentCount=0
    local badCount=0
    local goodText = ""
    local decentText = ""
    local badText = ""
    for k,v in pairs(ValidFood) do
        local count = GetItemCount(k)
        ValidFood[k].count=count
        if count > 0 then
            if (v.ilvl+20) >= petLvl then
                if lowestCount > count then
                    lowestCount = count
                    currentFood = k
                end
            elseif (v.ilvl+30) >= petLvl then
                if lowestShitFoodCount > count then
                    lowestShitFoodCount = count
                    lowestShitFood = k
                end
            end
        end
    end
    
    -- falling back to shitfood if no decent food available
    if currentFood == nil then
        currentFood = lowestShitFood
    end

    local newText = macroText
    if currentFood ~= nil then
        newText = newText.."\n/use "..currentFood
    end
    MacroButton:SetAttribute("macrotext", newText);
    if currentFood ~= oldCurrentFood then
        print("New feedpet macro:\n"..newText)
    end

    for k,v in pairs(ValidFood) do
        if v.count > 0 then
            if (v.ilvl+10) >= petLvl then
                goodCount = goodCount+v.count
                if k == currentFood then goodText = goodText.."\124cFFFFFFFF=> " end
                goodText = goodText.."\124cFF00E600"..string.format("%-4d",v.count)..k.."\n"
            elseif (v.ilvl+20) >= petLvl then
                if k == currentFood then decentText = decentText.."\124cFFFFFFFF=> " end
                decentCount = decentCount+v.count
                decentText = decentText.."\124cFFFF6D00"..string.format("%-4d",v.count)..k.."\n"
            elseif (v.ilvl+30) >= petLvl then
                if k == currentFood then badText = badText.."\124cFFFFFFFF=> " end
                badCount = badCount+v.count
                badText = badText.."\124cFFFF0000"..string.format("%-4d",v.count)..k.."\n"
            end
        end
    end

    local foodText = goodText..decentText..badText
    FoodOverviewFrame.text:SetText(foodText)

    FoodOverviewFrame:SetWidth(FoodOverviewFrame.text:GetStringWidth()+12)
    FoodOverviewFrame:SetHeight(FoodOverviewFrame.text:GetStringHeight())
end

MacroButton:SetScript("OnUpdate", function()
    if GetTime() < NextBagUpdate then return end
    NextBagUpdate = GetTime() + 1
    if WaitingForBagCheck == false or InCombatLockdown() then return end
    WaitingForBagCheck = false
    SetNewFood()
end)

MacroButton:SetScript("OnEvent", function(self, event, ...)
    if event == "PLAYER_ENTERING_WORLD" then
        SetBinding("SHIFT-D", "CLICK FeedButton:LeftButton")
        WaitingForBagCheck = true
    elseif event == "BAG_UPDATE" then
        local bag = ...
        if bag > 3 then return end -- ignore quiver
        WaitingForBagCheck = true
    end
end)

