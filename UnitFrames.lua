-- Author      : G3m7
-- Create Date : 5/31/2019 7:24:06 PM

function GUF_InitUnitFrames()
    PlayerFrame:ClearAllPoints()
    PlayerFrame:SetUserPlaced(true)
    PlayerFrame:SetPoint("CENTER", nil, "CENTER", -288, -199)
    
    TargetFrame:ClearAllPoints()
    TargetFrame:SetUserPlaced(true)
    TargetFrame:SetPoint("CENTER", nil, "CENTER", 200, -199)
    
    CastingBarFrame:ClearAllPoints()
    CastingBarFrame:SetPoint("BOTTOM", PlayerFrame, "BOTTOM", 245, -50)
    ----------------------------------------------------

    CompactRaidFrameManager:ClearAllPoints()
    CompactRaidFrameManager:SetPoint("TOPLEFT", UIParent, "TOPLEFT", -182, -550)

    PetFrame:SetPoint("TOPLEFT", PlayerFrame, "TOPLEFT", 50, -60)
    PetFrame:SetScale(1.2)

    BuffFrame:SetScale(1.1)
end

hooksecurefunc("CompactRaidFrameManager_Expand", function(self)
        local point, relativeTo, relativePoint, xOfs, yOfs = self:GetPoint(1)
        self:SetPoint("TOPLEFT", UIParent, "TOPLEFT", -7, -550)
end)
hooksecurefunc("CompactRaidFrameManager_Collapse", function(self)
        local point, relativeTo, relativePoint, xOfs, yOfs = self:GetPoint(1)
        self:SetPoint("TOPLEFT", UIParent, "TOPLEFT", -182, -550)
end)


