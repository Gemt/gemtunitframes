
-- Many functions here are copied from Leatrix Plus 1.13.03 (30th May 2019, www.leatrix.com)

function GUF_InitChatFrame()
------------------------------------------
-- increase chat history, leatrix_plus
------------------------------------------
for i = 1, 50 do
	if _G["ChatFrame" .. i] and _G["ChatFrame" .. i]:GetMaxLines() ~= 4096 then
		_G["ChatFrame" .. i]:SetMaxLines(4096);
	end
end
-- Process temporary chat frames
hooksecurefunc("FCF_OpenTemporaryWindow", function()
	local cf = FCF_GetCurrentChatFrame():GetName() or nil
	if cf then
		if (_G[cf]:GetMaxLines() ~= 4096) then
			_G[cf]:SetMaxLines(4096);
		end
	end
end)
------------------------------------------



----------------------------------------------------------------------
--	Move chat editbox to top, leatrix_plus
----------------------------------------------------------------------

-- Set options for normal chat frames
for i = 1, 50 do
	if _G["ChatFrame" .. i] then
		-- Position the editbox
		_G["ChatFrame" .. i .. "EditBox"]:ClearAllPoints();
		_G["ChatFrame" .. i .. "EditBox"]:SetPoint("TOPLEFT", _G["ChatFrame" .. i], 0, 35);
		_G["ChatFrame" .. i .. "EditBox"]:SetWidth(_G["ChatFrame" .. i]:GetWidth());
		-- Ensure editbox width matches chatframe width
		_G["ChatFrame" .. i]:HookScript("OnSizeChanged", function()
			_G["ChatFrame" .. i .. "EditBox"]:SetWidth(_G["ChatFrame" .. i]:GetWidth())
		end)

	end
end

-- Do the functions above for other chat frames (pet battles, whispers, etc)
hooksecurefunc("FCF_OpenTemporaryWindow", function()

	local cf = FCF_GetCurrentChatFrame():GetName() or nil
	if cf then

		-- Position the editbox
		_G[cf .. "EditBox"]:ClearAllPoints();
		_G[cf .. "EditBox"]:SetPoint("TOPLEFT", cf, "TOPLEFT", 0, 35);
		_G[cf .. "EditBox"]:SetWidth(_G[cf]:GetWidth());

		-- Ensure editbox width matches chatframe width
		_G[cf]:HookScript("OnSizeChanged", function()
			_G[cf .. "EditBox"]:SetWidth(_G[cf]:GetWidth())
		end)

	end
end)
----------------------------------------------------------------------


----------------------------------------------------------------------
-- Unclamp chat frame, leatrix_plus
----------------------------------------------------------------------
-- Process normal and existing chat frames on startup
for i = 1, 50 do
	if _G["ChatFrame" .. i] then 
		_G["ChatFrame" .. i]:SetClampRectInsets(0, 0, 0, 0);
	end
end

-- Process new chat frames and combat log
hooksecurefunc("FloatingChatFrame_UpdateBackgroundAnchors", function(self)
	self:SetClampRectInsets(0, 0, 0, 0);
end)

-- Process temporary chat frames
hooksecurefunc("FCF_OpenTemporaryWindow", function()
	local cf = FCF_GetCurrentChatFrame():GetName() or nil
	if cf then
		_G[cf]:SetClampRectInsets(0, 0, 0, 0);
	end
end)
----------------------------------------------------------------------

----------------------------------------------------------------------
--	Use arrow keys in chat, leatrix_plus
----------------------------------------------------------------------
-- Enable arrow keys for normal and existing chat frames
for i = 1, 50 do
	if _G["ChatFrame" .. i] then
		_G["ChatFrame" .. i .. "EditBox"]:SetAltArrowKeyMode(false)
	end
end
-- Enable arrow keys for temporary chat frames
hooksecurefunc("FCF_OpenTemporaryWindow", function()
	local cf = FCF_GetCurrentChatFrame():GetName() or nil
	if cf then
		_G[cf .. "EditBox"]:SetAltArrowKeyMode(false)
	end
end)
----------------------------------------------------------------------

local chatWidth = 450
local chatHeight = 220

hooksecurefunc("FCF_RestorePositionAndDimensions", function(...)
	ChatFrame1:ClearAllPoints()
    ChatFrame1:SetUserPlaced(true)
    ChatFrame1:SetWidth(chatWidth)
    ChatFrame1:SetHeight(chatHeight)
    ChatFrame1:SetPoint("BOTTOMLEFT", UIParent, "BOTTOMLEFT",  5, 10)
end)

ChatFrame1:SetClampedToScreen(false)

ChatFrame1:ClearAllPoints()
ChatFrame1:SetUserPlaced(true)
ChatFrame1:SetWidth(chatWidth)
ChatFrame1:SetHeight(chatHeight)
ChatFrame1:SetPoint("BOTTOMLEFT", UIParent, "BOTTOMLEFT",  5, 10)
SetChatWindowColor(1,0,0,0)
SetChatWindowAlpha(1, 1)
ChatFrame1ButtonFrame:ClearAllPoints()

ChatFrame1ButtonFrame:SetPoint("TOPRIGHT", ChatFrame1Background, "TOPRIGHT", 30, 0)
ChatFrame1ButtonFrame:SetPoint("BOTTOMRIGHT", ChatFrame1Background, "BOTTOMRIGHT", 0, 0)
    
    
end