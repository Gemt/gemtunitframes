-- Author      : G3m7
-- Create Date : 6/26/2019 7:12:58 PM

local frame = CreateFrame("frame")
frame:RegisterEvent("PLAYER_LOGIN")
frame:SetScript("OnEvent", function(self, event)
    GameTooltipStatusBar:ClearAllPoints()
    GameTooltipStatusBar:SetPoint("BOTTOMLEFT", GameTooltip, "TOPLEFT", 2, 1)
    GameTooltipStatusBar:SetPoint("BOTTOMRIGHT", GameTooltip, "TOPRIGHT", -2, 1)
end)

hooksecurefunc("GameTooltip_SetDefaultAnchor", function(tooltip, parent)
    tooltip:SetOwner(parent, "ANCHOR_NONE");
    tooltip:SetPoint("BOTTOMRIGHT", "UIParent", "BOTTOMRIGHT", 0, 0);
    tooltip.default = 1;
    frame.txt = GameTooltipStatusBar:CreateFontString("OVERLAY", nil, "NumberFontNormalSmallGray")
    frame.txt:SetPoint("LEFT", GameTooltipStatusBar, "CENTER", 0, 0)
    frame.txt:SetTextColor(1,1,1,1)
end)

local function SetHealthValues(unit)
	local health = UnitHealth(unit)
	local maxHealth = UnitHealthMax(unit)
	healthBar.bar:SetMinMaxValues(0, maxHealth)
	healthBar.bar:SetValue(health)
end

local lastUpdate = 0
local updateFrequency = 0.5

GameTooltip:HookScript("OnUpdate", function(self, elapsed)
	if (GameTooltipStatusBar:IsShown()) and (UnitExists(frame.unit)) then
		lastUpdate = lastUpdate + elapsed
		if (lastUpdate > updateFrequency) then
            local health = UnitHealth(frame.unit)
	        local maxHealth = UnitHealthMax(frame.unit)
            local pct = 100/maxHealth*health
			frame.txt:SetText(string.format("%d", pct))
			lastUpdate = 0
		end
	end
end)

GameTooltip:HookScript("OnTooltipSetUnit", function(self)
    local _, unit = self:GetUnit()
    frame.unit = unit
end)
