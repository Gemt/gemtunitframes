-- Author      : G3m7
-- Create Date : 5/31/2019 7:46:27 PM
function GUF_InitWorldMap()
if not IsAddOnLoaded("Blizzard_WorldMap") then
    LoadAddOn("Blizzard_WorldMap")            
end
WorldMapFrame.BlackoutFrame:Hide()

WorldMapFrame:ClearAllPoints()
WorldMapFrame:SetMovable(true)
WorldMapFrame:EnableMouse(true)
WorldMapFrame:RegisterForDrag("LeftButton")
WorldMapFrame:SetUserPlaced(true)
WorldMapFrame:EnableMouseWheel(true)

WorldMapContinentDropDown:Hide()
WorldMapZoneDropDown:Hide()
WorldMapZoomOutButton:Hide()
WorldMapMagnifyingGlassButton:Hide()
WorldMapFrame.BorderFrame:Hide()

WorldMapFrame.ScrollContainer:EnableMouse(true)
WorldMapFrame.ScrollContainer:RegisterForDrag("LeftButton")
WorldMapFrame.ScrollContainer:EnableMouseWheel(true)


WorldMapFrame:SetScript("OnDragStart", function(self)
    self:StartMoving()
end)

WorldMapFrame:SetScript("OnDragStop", function(self)
    self:StopMovingOrSizing()
end)

WorldMapFrame:SetScript('OnMouseWheel', function(self, direction)
    if IsAltKeyDown() then
        local currScale = WorldMapFrame:GetScale()
        if direction > 0 then
            WorldMapFrame:SetScale(math.min(currScale + 0.1, 2))
        else
            WorldMapFrame:SetScale(math.max(currScale - 0.1, 0.1))
        end
    end
end)

WorldMapFrame.ScrollContainer:SetScript('OnMouseWheel', function(self, direction)
    if IsAltKeyDown() then
        local currScale = WorldMapFrame:GetScale()
        if direction > 0 then
            WorldMapFrame:SetScale(math.min(currScale + 0.1, 2))
        else
            WorldMapFrame:SetScale(math.max(currScale - 0.1, 0.1))
        end
    end
end)

WorldMapFrame.ScrollContainer:SetScript("OnDragStart", function(self)
    WorldMapFrame:StartMoving()
end)

WorldMapFrame.ScrollContainer:SetScript("OnDragStop", function(self)
    WorldMapFrame:StopMovingOrSizing()
end)

WorldMapFrame.ScrollContainer.GetCursorPosition = function(f)
    local x,y = MapCanvasScrollControllerMixin.GetCursorPosition(f);
    local s = WorldMapFrame:GetScale();
    return x/s, y/s;
end

end