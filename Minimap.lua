-- Author      : G3m7
-- Create Date : 5/31/2019 6:51:48 PM
if(not IsAddOnLoaded("Blizzard_TimeManager")) then
	LoadAddOn("Blizzard_TimeManager")
end

MinimapToggleButton:Hide()
Minimap:SetScale(1.5)
Minimap:SetClampedToScreen(true)
-- Set Map Mask
function GetMinimapShape()
    return 'SQUARE'
end

Minimap:SetMaskTexture("Interface\\Buttons\\WHITE8X8")
MinimapBorder:Hide()

-- Clear Default Anchors / Enable Movable Map
Minimap:ClearAllPoints()
Minimap:SetMovable(true)
Minimap:EnableMouse(true)
Minimap:RegisterForDrag("LeftButton")
Minimap:SetScript("OnDragStart", function(self)
    if IsAltKeyDown() then self:StartMoving()
    end
end)

Minimap:SetScript("OnDragStop", function(self)
    self:StopMovingOrSizing()
end)

Minimap:SetUserPlaced(true)

Minimap:SetPoint('TOPLEFT', UIParent, 0, 0)

-- Set Map Boarder Texture
MinimapBackdrop:SetBackdrop({
    bgFile = "Interface\\Tooltips\\UI-Tooltip-Background",
    edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border",
    tile = true, tileSize = 16, edgeSize = 16,
    insets = {left = 3, right = 3, top = 3, bottom = 3}
})

MinimapBackdrop:ClearAllPoints()
MinimapBackdrop:SetBackdropBorderColor(0.75, 0.75, 0.75)
MinimapBackdrop:SetBackdropColor(0.15, 0.15, 0.15, 0.0)
MinimapBackdrop:SetAlpha(1.0)
MinimapBackdrop:SetPoint("TOPLEFT", Minimap, "TOPLEFT", -4, 4)
MinimapBackdrop:SetPoint("BOTTOMRIGHT", Minimap, "BOTTOMRIGHT", 4, -4)

-- Enable Mouse Wheel Scrolling
Minimap:EnableMouseWheel(true)
Minimap:SetScript('OnMouseWheel', function(self, direction)
    if IsAltKeyDown() then
        if direction > 0 then
            Minimap:SetScale(Minimap:GetScale() + 0.1)
        else
            Minimap:SetScale(Minimap:GetScale() - 0.1)
        end
    else
        local zoom, maxZoom = self:GetZoom() + direction, self:GetZoomLevels()
        self:SetZoom(zoom >= maxZoom and maxZoom or zoom <= 0 and 0 or zoom)
    end
end)

-- Hide Objects
MinimapZoomIn:Hide()            -- Zoom In Button
MinimapZoomOut:Hide()           -- Zoom Out Button
MinimapNorthTag:SetTexture()    -- North Compass Pointer
MinimapBorderTop:Hide()         -- Zone Name Border
MiniMapWorldMapButton:Hide() 	-- World Map Button

--  Zone Name Placement
MinimapZoneTextButton:ClearAllPoints()
MinimapZoneTextButton:SetPoint("CENTER", Minimap, "BOTTOM", 0, -10)
MinimapZoneTextButton:SetScale(1)
MinimapZoneTextButton:SetFrameStrata("HIGH")

-- Calendar Button Placement
--GameTimeFrame:ClearAllPoints()
--GameTimeFrame:SetPoint("TOPRIGHT", Minimap, 50, 10)
--GameTimeFrame:SetScale(.75)
--GameTimeFrame:SetFrameStrata("HIGH")
GameTimeFrame:Hide()

--  Mail Button Placement
MiniMapMailFrame:ClearAllPoints()
MiniMapMailFrame:SetPoint("TOPRIGHT", Minimap, 32, -20)
MiniMapMailFrame:SetScale(0.85)
MiniMapMailFrame:SetFrameStrata("HIGH")

-- Help Ticket Button Placement
HelpOpenWebTicketButton:ClearAllPoints()
HelpOpenWebTicketButton:SetPoint("TOPRIGHT", Minimap, 30, -100)
HelpOpenWebTicketButton:SetScale(1)
HelpOpenWebTicketButton:SetFrameStrata("HIGH")

-- tracking frame
MiniMapTrackingFrame:ClearAllPoints()
MiniMapTrackingFrame:SetPoint("BOTTOMRIGHT", Minimap, 32, -20)
MiniMapTrackingFrame:SetScale(0.75)
MiniMapTrackingFrame:SetFrameStrata("HIGH")

-- Clock
TimeManagerClockButton:SetPoint("TOP", Minimap, "TOP", 0, 5)
TimeManagerClockButton:SetPoint("LEFT", Minimap, "RIGHT", -8, 0)



--/run TimeManagerClockButton:SetPoint("TOP", _G["Minimap"], "TOP", -8, -20)